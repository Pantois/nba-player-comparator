<?php

namespace App\Entity;

use App\Repository\PlayerRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PlayerRepository::class)
 */
class Player
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $LastName;

    /**
     * @ORM\Column(type="string", length=2)
     */
    private $position;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $height_feet;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $height_inches;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $weight_pounds;

    /**
     * @ORM\ManyToOne(targetEntity=Team::class, cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $team;

    /**
     * @ORM\ManyToOne(targetEntity=Comparaison::class, inversedBy="players")
     * @ORM\JoinColumn(nullable=false)
     */
    private $comparaison;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $NbGame;

    private function setId(int $newId): self
    {

        $this->id = $newId;
        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->LastName;
    }

    public function setLastName(string $LastName): self
    {
        $this->LastName = $LastName;

        return $this;
    }

    public function getPosition(): ?string
    {
        return $this->position;
    }

    public function setPosition(string $position): self
    {
        $this->position = $position;

        return $this;
    }

    public function getHeightFeet(): ?int
    {
        return $this->height_feet;
    }

    public function setHeightFeet(?int $height_feet): self
    {
        $this->height_feet = $height_feet;

        return $this;
    }

    public function getHeightInches(): ?int
    {
        return $this->height_inches;
    }

    public function setHeightInches(?int $height_inches): self
    {
        $this->height_inches = $height_inches;

        return $this;
    }

    public function getWeightPounds(): ?int
    {
        return $this->weight_pounds;
    }

    public function setWeightPounds(?int $weight_pounds): self
    {
        $this->weight_pounds = $weight_pounds;

        return $this;
    }

    public function getTeam(): ?Team
    {
        return $this->team;
    }

    public function setTeam(?Team $team): self
    {
        $this->team = $team;

        return $this;
    }


    public static function setFromArray(array $from): Player
    {
        $p = new Player();
        $p->setid($from['id'])
            ->setTeam(Team::setFromArray($from['team']))
            ->setFirstName($from["first_name"])
            ->setHeightFeet($from["height_feet"])
            ->setHeightInches($from["height_inches"])
            ->setLastName($from["last_name"])
            ->setPosition($from["position"])
            ->setWeightPounds($from["weight_pounds"]);
        return $p;
    }

    public function getComparaison(): ?Comparaison
    {
        return $this->comparaison;
    }

    public function setComparaison(?Comparaison $comparaison): self
    {
        $this->comparaison = $comparaison;

        return $this;
    }

    public function getNbGame(): ?int
    {
        return $this->NbGame;
    }

    public function setNbGame(?int $NbGame): self
    {
        $this->NbGame = $NbGame;

        return $this;
    }
}
