<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use App\Repository\ComparaisonRepository;

class HistoryController extends AbstractController
{
    /**
     * @Route("/history", name="app_history")
     */
    public function index(ComparaisonRepository $compRep): Response
    {
        return $this->render('history/index.html.twig', [
            'comparaisons' => $compRep->findAll(),
        ]);
    }
}
