<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;

use App\Service\CallApiService;
use App\Repository\TeamRepository;
use App\Repository\PlayerRepository;
use App\Entity\Comparaison;

use App\Repository\ComparaisonRepository;

class ComparaisonController extends AbstractController
{
    private const COOKIES_PLAYER_SELECTED = 'playersSelected';
    private $list_selected_players;
    private $selected_value_raw;


    public function cookiesReader(Request $request, CallApiService $callApiService): void 
    {
        $this->selected_value_raw = $request->cookies->get(ComparaisonController::COOKIES_PLAYER_SELECTED, '');
        if ($this->selected_value_raw !== ''){
            $tab = explode(',', $this->selected_value_raw);
            $this->list_selected_players = [];
            foreach ($tab as $value) {
                if ($value !== '')
                    array_push($this->list_selected_players, $callApiService->getPlayer(intval($value)));
            }
        }
        else {
            $this->list_selected_players = [];
        }
    }

    /**
     * @Route("/comparaison", name="app_comparaison")
     */
    public function index(Request $request, CallApiService $callApiService, EntityManagerInterface $em, ManagerRegistry $registry): Response
    {
        $this->cookiesReader($request, $callApiService);
        $comp = new Comparaison();
        foreach ($this->list_selected_players as $player) {
            $player->setNbGame($callApiService->getAllStat(($player->getId())));
            $comp->addPlayer($player);
        }
        if (sizeof($comp->getPlayers()) > 1) {
            // $res = $em->getRepository("App\Entity\Comparaison")->findOneBy(array(
            //     "players" => $comp->getPlayers()
            // ));


            // if (!isset($res)) {
                $em->persist($comp);
                $em->flush();
            // }
        }


        return $this->render('comparaison/index.html.twig', [
            'players' => $this->list_selected_players,
            'button_player' => false,
        ]);
    }

    /**
     * @Route("/comparaison/{id}", name="app_comparaison_hist")
     */
    public function indexHistoryComparaion($id, ComparaisonRepository $compRep): Response
    {
        $comp = $compRep->find($id);
        if (!isset($comp)){
            return $this->notFound();
        }
        
        $this->list_selected_players = $comp->getPlayers();

        return $this->render('comparaison/index.html.twig', [
            'players' => $this->list_selected_players,
            'button_player' => false,
        ]);
    }

    /**
     * @Route("/comparaison/NotFound", name="app_comparaison_not_found")
     */
    public function notFound(): Response
    {
        return $this->render('comparaison/not_found.html.twig');
    }
}
