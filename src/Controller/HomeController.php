<?php

namespace App\Controller;

use App\Service\CallApiService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;


use App\Entity\Research;
use App\Form\ResearchType;


class HomeController extends AbstractController
{
    private $form;
    

    private function configureForm(Request $request): ?Response
    {
        $research = new Research();
        $this->form = $this->createForm(ResearchType::class, $research);

        $this->form->handleRequest($request);
        
        if ($this->form->isSubmitted() && $this->form->isValid()) {
            $research = $this->form->getData();
            return $this->redirectToRoute('app_search', array ('name' => $research->getName()));
        }
        return null;
    }

    /**
     * @Route("/", name="app_homespace")
     */
    public function index(CallApiService $callApiService, Request $request, EntityManagerInterface $em): Response
    {
        return $this->indexHome($callApiService, $request, $em);
    }

    /**
     * @Route("/home/{page}", name="app_home", requirements={"page"="\d+"})
     */
    public function indexHome(CallApiService $callApiService, Request $request, EntityManagerInterface $em, int $page = 1): Response
    {
        $res = $this->configureForm($request);
        if ($res != null) {
            return $res;
        }

        return $this->render('home/index.html.twig', [
            'data' => $callApiService->getAllPlayers($page),
            'page' => $page,
            'last_page' => $callApiService->getNbPages(),
            'form' => $this->form->createView(),
            'url' => '/home/',
        ]);
    }

    /**
     * @Route("/search/{name}/{page}", name="app_search", requirements={"page"="\d+"})
     */
    public function indexSearch(CallApiService $callApiService, string $name, Request $request, EntityManagerInterface $em, int $page = 1): Response
    {
        $res = $this->configureForm($request);
        if ($res != null) {
            return $res;
        }


        return $this->render('home/index.html.twig', [
            'data' => $callApiService->getResearch($name, $page),
            'page' => $page,
            'last_page' => $callApiService->getNbPages(),
            'form' => $this->form->createView(),
            'url' => '/search/'.$name.'/',
        ]);
    }

    
}
