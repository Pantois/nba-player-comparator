<?php

namespace App\Service;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use App\Entity\Player;
use App\Repositories\PlayerRepository;
use Symfony\Component\BrowserKit\CookieJar;

class CallApiService
{
    private $client;
    private $nb_page;
    private array $playersIdSelected;


    public function getPlayersIdSelected(): array {
        return $this->playersIdSelected;
    }

    public function getPlayer(int $id):  Player
    {
        return $this->callPlayer('players/'.$id)[0];
    }

    public function getNbPages(): int
    {
        return $this->nb_page;
    }

    public function __construct(HttpClientInterface $client){
        $this->client = $client;
    }


    public function getResearch(string $name, int $page = 1){
        return $this->callPlayer('players?search='.htmlspecialchars($name).'&page='.$page);
        
    }

    public function getAllPlayers($page = 1): array
    {
        
        return $this->callPlayer('players?page='.$page);
    }
    
    public function getAllStat(int $id): int
    {
        return $this->callStat('stats?player_ids[]='.$id)['meta']['total_count'];
    }

    private function callPlayer(string $value): array{
        $reponse = $this->client->request(
            'GET',
            'https://www.balldontlie.io/api/v1/'.$value
        );
        $res = [];
        if (!isSet($reponse->toArray()['data'])){
            array_push($res, Player::setFromArray($reponse->toArray()));
        }
        else{
            foreach ($reponse->toArray()['data'] as $value) {
                array_push($res, Player::setFromArray($value));
            }
        }
        if (isSet($reponse->toArray()['meta']))
            $this->nb_page = $reponse->toArray()['meta']['total_pages'];
        return $res;
    }

    private function callStat($value): array
    {
        $reponse = $this->client->request(
            'GET',
            'https://www.balldontlie.io/api/v1/'.$value
        );
        return $reponse->toArray();
    }
}