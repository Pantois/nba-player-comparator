# NBA player comparator

Ce site permet de comparer des joueurs de NBA selon des critères simples

## install 
### requires

require 
* composer
* PHP 7.4
* Symfony 5.4

```bash
composer install
```


### run the server

```bash
symfony server:start
```
