let COOKIES_PLAYER_SELECTED = 'playersSelected';

function writeCookieSelectionAdd(id) {
    document.getElementById(id + '-button-add').style.visibility = 'hidden';
    document.getElementById(id + '-button-remove').style.visibility = 'visible';
    let lst_c = document.cookie.split(';');
    let other_c = '';
    let oldValue;
    let res;
    lst_c.forEach(element => {
        res = element.split('=');
        if (res[0] === COOKIES_PLAYER_SELECTED) {
            console.log(res[1]);
            oldValue = res[1];
            oldValue.split(',').forEach(elt => {
                if (elt === id) {
                    console.log(document.cookie);
                    return;
                }
            });
        } else {
            other_c += ";" + element;
        }
    });
    if (res[1] != null)
        document.cookie = COOKIES_PLAYER_SELECTED + "=" + id + ',' + oldValue;
    else
        document.cookie = COOKIES_PLAYER_SELECTED + "=" + id;
    console.log(document.cookie);
}

function writeCookieSelectionRemove(id) {
    document.getElementById(id + '-button-add').style.visibility = 'visible';
    document.getElementById(id + '-button-remove').style.visibility = 'hidden';
    let newValue = '';
    let lst_c = document.cookie.split(",");
    let other_c = '';
    lst_c.forEach(element => {
        let res = element.split('=');
        if (res[0] === COOKIES_PLAYER_SELECTED) {
            oldValue = res[1].split(',');
            for (let index = 0; index < oldValue.length; index++) {
                const element = oldValue[index];
                if (element === id) {
                    return;
                }
            }
        } else {
            other_c += ";" + element;
        }
    });
    document.cookie = COOKIES_PLAYER_SELECTED + "=" + newValue + other_c;
    console.log(document.cookie);
}